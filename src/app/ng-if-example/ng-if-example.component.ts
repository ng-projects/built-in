import { Component, OnInit, HostBinding } from '@angular/core';

@Component({
  selector: 'app-ng-if-example',
  templateUrl: './ng-if-example.component.html',
  styleUrls: ['./ng-if-example.component.css']
})
export class NgIfExampleComponent implements OnInit {
  @HostBinding("attr.class") cssClas = "component-container";
  myVar: string = "lala";
  str: string = "yes";
  a: number = 5;
  b: number = 10;

  constructor() { }

  ngOnInit() {
  }

  myFunc() : boolean {
    return true;
  }

}
