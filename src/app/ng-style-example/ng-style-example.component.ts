import { Component, OnInit, HostBinding} from '@angular/core';

@Component({
  selector: 'app-ng-style-example',
  templateUrl: './ng-style-example.component.html',
  styleUrls: ['./ng-style-example.component.css']
})
export class NgStyleExampleComponent implements OnInit {
  @HostBinding("attr.class") cssClass = "component-container";
  color: string = "#ABCDEF";
  fontSize: number = 12;

  constructor() { }

  apply(color: string, fontSize: number): void {
    console.log("color", color);
    this.color = color;
    this.fontSize = fontSize;
  }
  

  ngOnInit() {
  }

}
