import { Component, OnInit, HostBinding } from '@angular/core';


@Component({
  selector: 'app-ng-for-example',
  templateUrl: './ng-for-example.component.html',
  styleUrls: ['./ng-for-example.component.css']
})
export class NgForExampleComponent implements OnInit {

  @HostBinding("attr.class") cssClass = "component-container";
  cities: Array<string> = ["Seatle", "Carapicuíba", "Londres", "São Roque", "Berlin", "Osasco"];
  people: Array<Object> = [
    {name: "Anderson", age: 35, city: "São Paulo"},
    {name: "Jonh", age: 15, city: "Miami"},
    {name: "Peter", age: 5, city: "New York"},
    {name: "Carl", age: 22, city: "Osasco"},
    {name: "Arnold", age: 17, city: "Piraporinha"}
  ]

  peopleByCity: Array<Object> = [
    {
      city: "Miami",
      people: [
        {name: "John", age: 12},
        {name: "Lawrence", age: 23},
      ]
    },
    {
      city: "São Paulo",
      people: [
       {name: "Anderson", age: 32},
       {name: "Jailson", age: 58} 
      ]
    }
  ]
  constructor() { }

  ngOnInit() {
  }


}
