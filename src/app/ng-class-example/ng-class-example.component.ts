import { Component, OnInit, HostBinding} from '@angular/core';

@Component({
  selector: 'app-ng-class-example',
  templateUrl: './ng-class-example.component.html',
  styleUrls: ['./ng-class-example.component.css']
})
export class NgClassExampleComponent implements OnInit {
  @HostBinding("attr.class") cssClass = "component-container";
  isBordered: boolean;
  classesObj: Object = {bordered : false};
  classList: string[];

  constructor() { }

  ngOnInit() {
    this.isBordered =true;
    this.classList= ["blue", "round"];
    this.toggleBorder();
  }

  toggleBorder(): void {
    this.isBordered = !this.isBordered;
    this.classesObj = {
      bordered: this.isBordered
    }
  }

}
