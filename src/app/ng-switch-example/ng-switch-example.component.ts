import { Component, OnInit, HostBinding } from '@angular/core';

@Component({
  selector: 'app-ng-switch-example',
  templateUrl: './ng-switch-example.component.html',
  styleUrls: ['./ng-switch-example.component.css']
})
export class NgSwitchExampleComponent implements OnInit {

  @HostBinding("attr.class") cssClass = "component-container";

  choice: number = 1;

  constructor() { }

  ngOnInit() {
  }

  nextChoice() : void{
    this.choice++;
  }
  previousChoice() : void{
    this.choice--;
  }

}
